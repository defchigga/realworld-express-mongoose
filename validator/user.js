const validate = require('../middleware/validate')
const { body } = require('express-validator')
const { User } = require('../model')
const { md5 } = require('../utils')

// 用户注册校验
exports.register = validate([
  body('user.username')
    .notEmpty().withMessage('用户名不能为空')
    .custom(async (username) => {
      const user = await User.findOne({ username })
      if (user) {
        throw new Error('用户名已经存在')
      }
    }),
  body('user.password').notEmpty().withMessage('密码不能为空'),
  body('user.email')
    .notEmpty().withMessage('邮箱不能为空').bail() // bail 方法验证不通过不往下继续验证
    .isEmail().withMessage('邮箱格式不正确').bail()
    .custom(async (email) => {
      const user = await User.findOne({ email })
      if (user) {
        throw new Error('邮箱已经存在')
      }
    })
])

// 用户登录校验
exports.login = [
  validate([
    body('user.email')
      .notEmpty().withMessage('邮箱不能为空').bail()
      .isEmail().withMessage('邮箱格式不正确'),
    body('user.password').notEmpty().withMessage('密码不能为空')
  ]),
  /* 查库验证操作使用单独中间件编写 避免性能浪费 */
  // 验证邮箱是否存在
  validate([
    body('user.email').custom(async (email, { req }) => {
      const user = await User.findOne({ email })
      if (!user) {
        throw new Error('邮箱不存在')
      }
      // 把查询出来的 user 对象挂载到 req 上，以便于下个中间件使用（防止再次查询数据库，浪费性能）
      req.user = user
    })
  ]),
  // 验证密码是否匹配
  validate([
    body('user.password').custom(async (password, { req }) => {
      // 把用户密码加密后与数据库中密码比较
      if (md5(password) !== req.user.password) {
        throw new Error('密码错误')
      }
    })
  ])
]

// 更新用户校验
exports.updateCurrentUser = validate([
  body('user')
    .exists().withMessage('缺少 user 参数').bail()
    .isObject().withMessage('user 格式不正确').bail()
])
