const validate = require('../middleware/validate')
const { body, param, query } = require('express-validator')
const { isValidObjectId } = require('mongoose')

// 创建文章验证
exports.createArticle = [
  // 验证传入对象
  validate([
    body('article')
      .exists().withMessage('缺少 article 参数').bail()
      .isObject().withMessage('article 格式不正确')
  ]),
  // 验证传入对象的字段
  validate([
    body('article.title').notEmpty().withMessage('文章标题 title 不能为空'),
    body('article.description').notEmpty().withMessage('文章描述 description 不能为空'),
    body('article.body').notEmpty().withMessage('文章内容 body 不能为空').bail(),
    // 可选字段
    body('article.tagList').optional().isArray().withMessage('文章标签列表 tagList 必须为数组类型')
  ])
]

// 更新文章验证
exports.updateArticle = [
  // 验证 params 参数
  validate([
    param('slug')
      .notEmpty().withMessage('slug 参数不能为空').bail() // 验证传入的 slug 是否有值
    // 验证传入的 slug 是否为 ObjectId 格式
      .custom(async (slug) => {
        if (!isValidObjectId(slug)) {
          await Promise.reject(new TypeError('slug 不是一个有效的 ObjectId 类型'))
        }
      })
  ]),
  // 验证 body 参数
  validate([
    // 验证传入对象
    body('article')
      .exists().withMessage('缺少 article 参数').bail()
      .isObject().withMessage('article 格式不正确')
  ]),
  // 验证传入对象的可选字段
  validate([
    body('article.title').optional().notEmpty().withMessage('文章标题 title 不能为空'),
    body('article.description').optional().notEmpty().withMessage('文章描述 description 不能为空'),
    body('article.body').optional().notEmpty().withMessage('文章内容 body 不能为空').bail()
  ])
]

// 获取单个文章验证
exports.getArticleBySlug = validate([
  param('slug')
    .notEmpty().withMessage('slug 参数不能为空').bail() // 验证传入的 slug 是否有值
    // 验证传入的 slug 是否为 ObjectId 格式
    .custom(async (slug) => {
      if (!isValidObjectId(slug)) {
        await Promise.reject(new TypeError('slug 不是一个有效的 ObjectId 类型'))
      }
    })
])

// 删除文章验证
exports.deleteArticle = validate([
  param('slug')
    .notEmpty().withMessage('slug 参数不能为空').bail() // 验证传入的 slug 是否有值
    // 验证传入的 slug 是否为 ObjectId 格式
    .custom(async (slug) => {
      if (!isValidObjectId(slug)) {
        await Promise.reject(new TypeError('slug 不是一个有效的 ObjectId 类型'))
      }
    })
])

// 获取文章列表验证
exports.getArticles = validate([
  query('author').optional().notEmpty().withMessage('author 参数不能为空'),
  query('favorited').optional().notEmpty().withMessage('favorited 参数不能为空'),
  query('tag').optional().notEmpty().withMessage('tag 参数不能为空')
])

// 喜爱文章验证
exports.favoriteArticle = validate([
  param('slug')
    .notEmpty().withMessage('slug 参数不能为空').bail()
    .custom(async (slug) => {
      if (!isValidObjectId(slug)) {
        await Promise.reject(new TypeError('slug 不是一个有效的 ObjectId 类型'))
      }
    })
])

// 取消喜爱文章验证
exports.unFavoriteArticle = validate([
  param('slug')
    .notEmpty().withMessage('slug 参数不能为空').bail()
    .custom(async (slug) => {
      if (!isValidObjectId(slug)) {
        await Promise.reject(new TypeError('slug 不是一个有效的 ObjectId 类型'))
      }
    })
])
