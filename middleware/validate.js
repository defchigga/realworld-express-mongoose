const { validationResult } = require('express-validator')

// 并行执行 - https://express-validator.github.io/docs/running-imperatively.html
module.exports = validations => {
  return async (req, res, next) => {
    await Promise.all(validations.map(validation => validation.run(req)))

    const errors = validationResult(req)
    if (errors.isEmpty()) {
      return next()
    }

    res.status(400).json({ errors: errors.array() })
  }
}
