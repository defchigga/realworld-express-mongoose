/**
 * 路由配置
 */
const express = require('express')

const router = express.Router()

// 注册路由
router.use(require('./routes/user')) // 用户相关
router.use('/profiles', require('./routes/profile')) // 用户资料相关
router.use('/articles', require('./routes/article')) // 文章相关
router.use('/tags', require('./routes/tag')) // 标签相关

module.exports = router
