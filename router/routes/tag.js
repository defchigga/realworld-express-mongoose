const express = require('express')
const { getTags } = require('../../controller/tag')

const router = express.Router()

// 获取标签列表
router.get('/', getTags)

module.exports = router
