const express = require('express')
const { getProfile, followProfile, unFollowProfile } = require('../../controller/profile')

const router = express.Router()

// 获取用户资料
router.get('/:username', getProfile)

// 关注用户
router.post('/:username/follow', followProfile)

// 取关用户
router.delete('/:username/follow', unFollowProfile)

module.exports = router
