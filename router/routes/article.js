const express = require('express')
const auth = require('../../middleware/auth')
const articleValidator = require('../../validator/article')
const articleController = require('../../controller/article')

const router = express.Router()

// 获取全局文章列表
router.get('/', articleValidator.getArticles, articleController.getArticles)

// 获取订阅文章列表
router.get('/feed', articleController.getFeedArticles)

// 获取单个文章
router.get('/:slug', auth, articleValidator.getArticleBySlug, articleController.getArticleBySlug)

// 创建文章
router.post('/', auth, articleValidator.createArticle, articleController.createArticle)

// 更新文章
router.put('/:slug', auth, articleValidator.updateArticle, articleController.updateArticle)

// 删除文章
router.delete('/:slug', auth, articleValidator.deleteArticle, articleController.deleteArticle)

// 向文章添加评论
router.post('/:slug/comments', articleController.addArticleComments)

// 从文章获取评论
router.get('/:slug/comments', articleController.getArticleComments)

// 删除评论
router.delete('/:slug/comments/:id', articleController.deleteArticleComments)

// 喜爱文章
router.post('/:slug/favorite', auth, articleValidator.favoriteArticle, articleController.favoriteArticle)

// 取消喜爱文章
router.delete('/:slug/favorite', auth, articleValidator.unFavoriteArticle, articleController.unFavoriteArticle)

module.exports = router
