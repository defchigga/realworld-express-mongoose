const express = require('express')
const logger = require('morgan')
const cors = require('cors')
const router = require('./router/index')
const errorHandler = require('./middleware/errorHandler')

const app = express()

// 配置常用中间件
app.use(logger('dev')) // HTTP 请求日志
app.use(express.json()) // 解析 JSON 格式数据并调用 JSON.parse 格式化赋值给 req.body 参数
app.use(express.urlencoded({ extended: false })) // 解析表单格式数据并调用 JSON.parse 格式化赋值给 req.body 参数
app.use(cors()) // CORS 跨域

// 配置路由
app.use('/api', router)

// 配置错误处理中间件
app.use(errorHandler())

const PORT = process.env.PORT || 3000
app.listen(PORT, () => {
  console.log(`The Server running at http://127.0.0.1:${PORT}`)
})
