// md5 加密
const crypto = require('crypto')

// 返回支持的哈希算法的名称数组
// console.log(crypto.getHashes())

module.exports = str => {
  return crypto.createHash('md5') // 使用 md5 加密算法
    .update('DefChigga' + str) // 加盐加密字符串
    .digest('hex') // 使用 16 进制
}
