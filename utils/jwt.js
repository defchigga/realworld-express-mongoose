// jwt 鉴权
const jwt = require('jsonwebtoken')
const { promisify } = require('util')

// 导出 promise 化方法
exports.sign = promisify(jwt.sign) // 签名
exports.verify = promisify(jwt.verify) // 验证
exports.decode = promisify(jwt.decode) // 解码
