/**
 * 模型
 */
const mongoose = require('mongoose')
const { dbUri } = require('../config/config.default')

// 导入 Schema
const userSchema = require('./user')
const articleSchema = require('./article')

// 连接 MongoDB 数据库
mongoose.connect(dbUri)

// 监听 MongoDB 连接状态
const db = mongoose.connection
db.on('error', err => {
  console.error('MongoDB 数据库连接失败', err)
})
db.once('open', () => {
  console.log('MongoDB 数据库连接成功')
})

// 统一导出模型
module.exports = {
  User: mongoose.model('User', userSchema),
  Article: mongoose.model('Article', articleSchema)
}
