const { Schema } = require('mongoose')
const baseModel = require('../model/base')

const articleSchema = Schema({
  ...baseModel,
  slug: Schema.Types.ObjectId, // 文章唯一标识
  title: { // 文章标题
    type: String,
    required: true
  },
  description: { // 文章描述
    type: String,
    required: true
  },
  body: { // 文章内容
    type: String,
    required: true
  },
  tagList: { // 文章标签
    type: [String],
    default: []
  },
  favorited: { // 是否喜爱
    type: Boolean,
    default: false
  },
  favoritesCount: { // 喜爱数量
    type: Number,
    default: 0
  },
  author: { // 作者信息
    type: Schema.Types.ObjectId, // 关联用户 id
    ref: 'User' // 引用用户模型
  }
  /* author: { // 作者
    username: 'jake', // 姓名
    bio: 'I work at statefarm', // 简介
    image: 'https://i.stack.imgur.com/xHWG8.jpg', // 头像
    following: false // 是否关注
  } */
})

module.exports = articleSchema
