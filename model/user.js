const mongoose = require('mongoose')
const baseModel = require('./base')
const { md5 } = require('../utils')

const userSchema = mongoose.Schema({
  ...baseModel,
  username: { // 用户名
    type: String,
    required: true
  },
  email: { // 邮箱
    type: String,
    required: true
  },
  password: { // 密码
    type: String,
    required: true,
    // 模型自动加密后再返回值
    set: value => md5(value) // set: 函数 使用 Object.defineProperty() 定义自定义 setter
  },
  bio: { // 简介
    type: String,
    default: null
  },
  image: { // 头像
    type: String,
    default: null
  },
  following: { // 是否关注
    type: Boolean,
    default: false
  }
})

module.exports = userSchema
