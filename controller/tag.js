// 获取标签列表
exports.getTags = (req, res, next) => {
  try {
    res.send('GET /tags')
  } catch (err) {
    next(err)
  }
}
