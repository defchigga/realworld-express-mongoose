const { Article } = require('../model')

// 获取全局文章列表处理
exports.getArticles = async (req, res, next) => {
  try {
    const { offset = 0, limit = 20, tag } = req.query
    const params = {}
    if (tag) {
      params.tagList = tag
    }
    // 获取文章总数
    const articlesCount = await Article.countDocuments()
    if (articlesCount < 1) {
      return res.status(200).json({ articles: [], articlesCount })
    }
    // 获取文章列表
    const articles = await Article
      .find(params).skip(offset).limit(limit)
      .populate({
        path: 'author',
        select: ['username', 'bio', 'image', 'following']
      })
    res.status(200).json({ articles, articlesCount })
  } catch (err) {
    next(err)
  }
}

// 获取订阅文章列表处理
exports.getFeedArticles = (req, res, next) => {
  try {
    res.send('GET /articles/feed')
  } catch (err) {
    next(err)
  }
}

// 获取单个文章处理
exports.getArticleBySlug = async (req, res, next) => {
  try {
    const article = await Article.findById(req.params.slug).populate({ // populate 查询时候带出作者信息对象
      path: 'author', // path 为 Schema.Types.ObjectId 关联用户字段
      select: ['username', 'bio', 'image', 'following'] // select 为查询的字段 默认查询全部
    })
    if (!article) {
      return res.status(404).end()
    }
    res.status(200).json({ article })
  } catch (err) {
    next(err)
  }
}

// 创建文章处理
exports.createArticle = async (req, res, next) => {
  try {
    const fields = ['title', 'description', 'body', 'tagList']
    const params = {}
    Object.entries(req.body.article).forEach(([key, value]) => {
      if (fields.includes(key)) {
        params[key] = value
      }
    })
    const article = new Article(params)
    article.slug = article._id
    article.author = req.user._id
    await article.save()
    res.status(201).json({ article })
  } catch (err) {
    next(err)
  }
}

// 更新文章处理
exports.updateArticle = async (req, res, next) => {
  try {
    const fields = ['title', 'description', 'body']
    const params = {}
    Object.entries(req.body.article).forEach(([key, value]) => {
      if (fields.includes(key)) {
        params[key] = value
      }
    })
    const article = await Article
      .findByIdAndUpdate(req.params.slug, params, {
        new: true // 返回修改后的最新文档
      })
      .populate({ // populate 查询时候带出作者信息对象
        path: 'author', // path 为 Schema.Types.ObjectId 关联用户字段
        select: ['username', 'bio', 'image', 'following'] // select 为查询的字段 默认查询全部
      })
    // 如果 article 为 null 说明没有找到对应文章
    if (!article) {
      return res.status(404).end()
    }
    res.status(200).json({ article })
  } catch (err) {
    next(err)
  }
}

// 删除文章处理
exports.deleteArticle = async (req, res, next) => {
  try {
    const article = await Article.findByIdAndDelete(req.params.slug)
    if (!article) {
      return res.status(404).end()
    }
    res.status(204).end()
  } catch (err) {
    next(err)
  }
}

// 向文章添加评论处理
exports.addArticleComments = (req, res, next) => {
  try {
    res.send(`POST /articles/${req.params.slug}/comments`)
  } catch (err) {
    next(err)
  }
}

// 从文章获取评论处理
exports.getArticleComments = (req, res, next) => {
  try {
    res.send(`GET /articles/${req.params.slug}/comments`)
  } catch (err) {
    next(err)
  }
}

// 删除评论处理
exports.deleteArticleComments = (req, res, next) => {
  try {
    res.send(`DELETE /articles/${req.params.slug}/comments/${req.params.id}`)
  } catch (err) {
    next(err)
  }
}

// 喜爱文章处理
exports.favoriteArticle = async (req, res, next) => {
  try {
    // 查询文章是否存在
    const article = await Article.findById(req.params.slug)
    if (!article) {
      return res.status(404).end()
    }
    if (article.favorited) return res.status(400).end('已经喜爱过了，请先取消喜爱')
    // 更新文章
    res.status(200).json({
      article: await Article.findByIdAndUpdate(req.params.slug, {
        // 修改喜爱状态
        $set: {
          favorited: true
        },
        // 增加喜爱数量
        $inc: {
          favoritesCount: 1
        }
      }, {
        new: true
      }).populate({ // populate 查询时候带出作者信息对象
        path: 'author', // path 为 Schema.Types.ObjectId 关联用户字段
        select: ['username', 'bio', 'image', 'following'] // select 为查询的字段 默认查询全部
      })
    })
  } catch (err) {
    next(err)
  }
}

// 取消喜爱文章处理
exports.unFavoriteArticle = async (req, res, next) => {
  try {
    // 查询文章是否存在
    const article = await Article.findById(req.params.slug)
    if (!article) {
      return res.status(404).end()
    }
    if (!article.favorited) return res.status(400).end('请先喜爱文章')
    // 更新文章
    res.status(200).json({
      article: await Article.findByIdAndUpdate(req.params.slug, {
        // 修改喜爱状态
        $set: {
          favorited: false
        },
        // 减少喜爱数量
        $inc: {
          favoritesCount: -1
        }
      }, {
        new: true
      }).populate({ // populate 查询时候带出作者信息对象
        path: 'author', // path 为 Schema.Types.ObjectId 关联用户字段
        select: ['username', 'bio', 'image', 'following'] // select 为查询的字段 默认查询全部
      })
    })
  } catch (err) {
    next(err)
  }
}
