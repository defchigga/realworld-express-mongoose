// 获取用户资料处理
exports.getProfile = (req, res, next) => {
  try {
    res.send(`GET /profiles/${req.params.username}`)
  } catch (err) {
    next(err)
  }
}

// 关注用户处理
exports.followProfile = (req, res, next) => {
  try {
    res.send(`POST /profiles/${req.params.username}/follow`)
  } catch (err) {
    next(err)
  }
}

// 取关用户处理
exports.unFollowProfile = (req, res, next) => {
  try {
    res.send(`DELETE /profiles/${req.params.username}/follow`)
  } catch (err) {
    next(err)
  }
}
