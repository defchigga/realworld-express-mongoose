const { User } = require('../model')
const { jwt } = require('../utils')
const { jwtSecret } = require('../config/config.default')

// 用户登录处理
exports.login = async (req, res, next) => {
  try {
    // 从用户登录验证中间件获取存储的 user 对象数据并格式化
    const user = req.user.toJSON()
    delete user.password
    // 生成 token
    user.token = await jwt.sign({
      userId: user._id // 存储标识字段 userId
    }, jwtSecret, {
      expiresIn: 60 * 60 * 24 // 过期时间为 1 天 '24h' '1d'
    })
    res.status(200).json({ user })
  } catch (err) {
    next(err)
  }
}

// 用户注册处理
exports.register = async (req, res, next) => {
  try {
    // 创建用户
    let user = await User.create(req.body.user)
    // 返回值去除密码
    user = user.toJSON() // 返回值 user 对象的值是从原型上查出来的 所以直接 delete key 是删除不了的 先使用 toJSON() 转换后再删除
    delete user.password
    // 成功响应
    res.status(201).json({ user })
  } catch (err) {
    next(err)
  }
}

// 获取当前用户处理
exports.getCurrentUser = (req, res, next) => {
  try {
    const user = req.user.toJSON()
    delete user.password
    res.status(200).json({
      user
    })
  } catch (err) {
    next(err)
  }
}

// 更新当前用户处理
exports.updateCurrentUser = async (req, res, next) => {
  try {
    // 参数处理
    const params = {}
    const keys = ['email', 'username', 'password', 'image', 'bio']
    for (const [key, value] of Object.entries(req.body.user)) {
      if (keys.includes(key) && value) params[key] = value
    }
    // 更新用户
    let user = await User.findByIdAndUpdate(req.user._id, params)
    user = user.toJSON()
    delete user.password
    delete params.password
    // 成功响应
    res.status(200).json({ user: { ...user, ...params } })
  } catch (err) {
    next(err)
  }
}
